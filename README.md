package weekoneassignment;

public class weekone extends naturalNumberClass {

	public static void main(String[] args)
	{
		System.out.println("Constructor:");
		naturalNumberClass n = new naturalNumberClass();
		int posVal = 5;
		int negVal = -10;
		System.out.println(n);
		System.out.print("Default natural value return be 0\nDefault natural value:  ");
		System.out.println(n.getNaturalValue());
		System.out.print("Update with positive input should return true\nUpdate with positive input:  ");
		System.out.println(n.updateNaturalValue(posVal));
		System.out.print("Get after Update with positive input should return the update value (5)\nGet after Update with positive input:  ");
		System.out.println(n.getNaturalValue());
		System.out.print("Update with negative input should return false\nUpdate with negative input:  ");
		System.out.println(n.updateNaturalValue(negVal));
		System.out.print("Get after Update with negative input should return the prior value(5)\nGet after Update with negative input:  ");
		System.out.println(n.getNaturalValue());
	}
}